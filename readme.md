# Skynin fanstasy sport (simple)

Проект основан на реальном фентези спорт проекте, написанном на Yii2.

Фронтенд написан полностью по другому, чем предлагает Laravel.  
Используется инлайнинг данных, вместо работы с API, require.js, riot.js, статические ссылки на ресурсы.  
Слоган реализации фронтенда - "Без вебпака!" ;)

Это нужно чтобы

 * Упростить натяжку кастомной верстки (верстальщик работает почти с такой же копией фронтенда, на которую выводятся фейковые данные)
 * Уменьшить трудозатраты внесения изменений в фронтенде
 * Обеспечить возможность внесения правок в код прямо на продакшн сервере

За основу взят: 

## Laravel 5.6 Boilerplate

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) 
[![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)
[![CircleCI](https://circleci.com/gh/rappasoft/laravel-5-boilerplate/tree/master.svg?style=svg)](https://circleci.com/gh/rappasoft/laravel-5-boilerplate/tree/master)

### License

MIT: [http://anthony.mit-license.org](http://anthony.mit-license.org)
